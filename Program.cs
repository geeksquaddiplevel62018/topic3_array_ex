﻿using System;

namespace comp6211_topic3_array_ex
{
    class Program
    {
        static void Main(string[] args)
        {
        /*
         Create two arrays of size 10:
            1.	Numeric type which can hold course IDs
            2.	String type that can hold course names

            Populate these arrays with dummy data and display them on screen.
            Perform following actions these arrays and display output on screen:

            •	Get the length-of both arrays
            •	Copy these arrays to new arrays
            •	Get the type of each array
            •	Get the values of each array at index 5
            •	Search for a specific value in an array and display the output
            •	Reverse the entire array
            •	Change the values of both arrays ay location 5.
            •	Sort the arrays in ascending and descending orders
        */

            // 1.	Numeric type which can hold course IDs
            // 2.	String type that can hold course names
            int[] arrNum = {10, 22,323,242,31,51,23,1,512,31};
            string[] arrStr = {"hello", "hi", "good", "morning", "sing", "love", "seven", "special", "normal", "anything"};

            int cntArrNum = arrNum.Length, cntArrStr = arrStr.Length;

            Console.WriteLine("------------------------------------");

            // Populate these arrays with dummy data and display them on screen.
            printArr(arrNum);
            printArr(arrStr);
            Console.WriteLine("------------------------------------\n");
            Console.ReadLine();
            Console.Clear();

            // •	Get the length-of both arrays
            Console.WriteLine("Lenth of array of Numeric type is : " + arrNum.Length);
            Console.WriteLine("Lenth of array of String type is : " + arrStr.Length);
            Console.WriteLine("------------------------------------\n");
            Console.ReadLine();
            Console.Clear();

            // •	Copy these arrays to new arrays
            int[] newArrNum = new int[arrNum.Length];
            Array.Copy(arrNum, newArrNum, arrNum.Length);
            Console.WriteLine("New numeric array is ... ");
            printArr(arrNum);
            Console.WriteLine("------------------------------------\n");

            string[] newArrStr = new string[arrStr.Length];
            Array.Copy(arrStr, newArrStr, arrStr.Length);
            Console.WriteLine("New string array is ... ");
            printArr(arrStr);
            Console.WriteLine("------------------------------------\n");
            Console.ReadLine();
            Console.Clear();

            // •	Get the type of each array
            Console.WriteLine("Type of nemeric array is : " + arrNum.GetType().GetElementType());
            Console.WriteLine("Type of string array is : " + arrStr.GetType().GetElementType());
            Console.WriteLine("------------------------------------\n");
            Console.ReadLine();
            Console.Clear();

            // •	Get the values of each array at index 5
            Console.WriteLine("The value of numeric array at index 5 is : " + arrNum[4]); 
            Console.WriteLine("The value of string array at index 5 is : " + arrStr[4]);
            Console.WriteLine("------------------------------------\n");
            Console.ReadLine();
            Console.Clear();

            // •	Search for a specific value in an array and display the output
            Console.Write("Type in a number what you search in numeric array : ");

            string strReaded = Console.ReadLine();
            int num;

            Int32.TryParse(strReaded, out num);

            Console.WriteLine("Result : ");
            int index  = Array.IndexOf(arrNum, num);

            if(index < 0)
            {
                Console.WriteLine("Sorry, there is not the number in the array.");
            }
            else
            {
                Console.WriteLine("The number is " + arrNum[index] + " at " + index + ".");
            }
            Console.WriteLine("------------------------------------\n");
            Console.ReadLine();
            Console.Clear();

            // For string array
            Console.Write("Type in a string what you search in string array : ");

            strReaded = Console.ReadLine();

            Console.WriteLine("Result : ");
            index  = Array.IndexOf(arrStr, strReaded);
            
            if(index < 0)
            {
                Console.WriteLine("Sorry, there is not the string in the array.");
            }
            else
            {
                Console.WriteLine("The string is " + arrStr[index] + " at " + index + ".");
            }
            Console.WriteLine("------------------------------------\n");
            Console.ReadLine();
            Console.Clear();

            // •	Reverse the entire array
            Console.WriteLine("Reverse : ");
            Array.Reverse(newArrNum);
            printArr(newArrNum);

            Array.Reverse(newArrStr);
            printArr(newArrStr);
            Console.WriteLine("------------------------------------\n");
            Console.ReadLine();
            Console.Clear();

            // •	Change the values of both arrays at location 5.
            Console.WriteLine("The number is " + newArrNum[4] + " at index 5.");
            newArrNum[4] = 100000;
            Console.WriteLine("The number changed is " + newArrNum[4] + " at index 5.");

            Console.WriteLine("The string is " + newArrStr[4] + " at index 5.");
            newArrStr[4] = "night";
            Console.WriteLine("The string changed is " + newArrStr[4] + " at index 5.");
            Console.WriteLine("------------------------------------\n");
            Console.ReadLine();
            Console.Clear();
            
            // •	Sort the arrays in ascending and descending orders
            Console.WriteLine("Sort numeric array by ascending.");
            Array.Sort(newArrNum);
            printArr(newArrNum);
            Console.WriteLine("Sort numeric array by descending.");
            Array.Reverse(newArrNum);
            printArr(newArrNum);

            Console.WriteLine("Sort string array by ascending.");
            Array.Sort(newArrStr);
            printArr(newArrStr);
            Console.WriteLine("Sort string array by descending.");
            Array.Reverse(newArrStr);
            printArr(newArrStr);
            Console.WriteLine("------------------------------------\n");
            Console.ReadLine();
            Console.Clear();

        }

        static void printArr(object arr)
        {
            if(arr.GetType() == typeof(int[]))
            {
                foreach(int num in arr as int[])
                {
                    Console.WriteLine(num);
                }
            }
            else if(arr.GetType() == typeof(string[]))
            {
                foreach(string str in arr as string[])
                {
                    Console.WriteLine(str);
                }
            }

            Console.WriteLine();
        }
    }
}
