** Create two arrays of size 10:
1.	Numeric type which can hold course IDs
2.	String type that can hold course names

** Populate these arrays with dummy data and display them on screen.
** Perform following actions these arrays and display output on screen:

•	Get the length-of both arrays
•	Copy these arrays to new arrays
•	Get the type of each array
•	Get the values of each array at index 5
•	Search for a specific value in an array and display the output
•	Reverse the entire array
•	Change the values of both arrays ay location 5.
•	Sort the arrays in ascending and descending orders